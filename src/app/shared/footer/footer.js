import React from 'react';
import './footer.scss';

const Footer = () => {
    return (
        <div className="footer d-flex justify-content-center align-items-center w-100">Created by Acvila Team</div>
    );
}

export default Footer;
